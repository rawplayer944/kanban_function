//DOM cache
let UI = {
    elBoard: createKanbanDiv(),
    elCardPlaceholder: null
  },
  lists = [],
  toDos = [],
  isDragging = false,
  _listCounter = 0,
  _cardCounter = 0;

// Vanilla js event listener
function live(eventType, selector, callback) {
  document.addEventListener(
    eventType,
    function(e) {
      if (e.target.webkitMatchesSelector(selector)) {
        callback.call(e.target, e);
      }
    },
    false
  );
}

live('dragstart', '.list .card', function(e) {
  isDragging = true;
  e.dataTransfer.setData('text/plain', e.target.dataset.id);
  e.dataTransfer.dropEffect = 'copy';
  e.target.classList.add('dragging');
});
live('dragend', '.list .card', function(e) {
  this.classList.remove('dragging');
  UI.elCardPlaceholder && UI.elCardPlaceholder.remove();
  UI.elCardPlaceholder = null;
  isDragging = false;
});

live('dragover', '.list, .list .card, .list .card-placeholder', function(e) {
  e.preventDefault();
  e.dataTransfer.dropEffect = 'move';
  if (this.className === 'list') {
    // List
    this.appendChild(getCardPlaceholder());
  } else if (this.className.indexOf('card') !== -1) {
    // Card
    this.parentNode.insertBefore(getCardPlaceholder(), this);
  }
});

live('drop', '.card, .list, .list .card-placeholder', function(e) {
  e.stopPropagation();
  e.preventDefault();
  if (
    e.target.className.indexOf('card') !== -1 &&
    e.target.className.indexOf('placeholder') === -1
  ) {
    return false;
  }
  let todo_id = +e.dataTransfer.getData('text');
  let todo = findListObject(toDos, todo_id, true);
  let newListID = null;
  if (this.className === 'list') {
    // Dropped on List
    newListID = this.dataset.id;
    this.appendChild(todo.dom);
  } else {
    newListID = this.parentNode.dataset.id;
    this.parentNode.replaceChild(todo.dom, this);
  }
  moveCard(todo_id, +newListID);
});

function createCard(cardDOMText, listID, index) {
  if (!cardDOMText || cardDOMText === '') return false;
  let newCardId = ++_cardCounter;
  let card = document.createElement('div');
  let list = findListObject(lists, listID, true);
  card.draggable = true;
  card.dataset.id = newCardId;
  card.dataset.listId = listID;
  card.id = 'todo_' + newCardId;
  card.className = 'card';
  card.addEventListener('mouseup', () => {
    if (!isDragging) {
      alert(`You clicked on ${card.id}`);
    }
  });
  card.innerHTML = cardDOMText.trim();
  let todo = {
    _id: newCardId,
    listID: listID,
    text: cardDOMText,
    dom: card,
    index: index || list.cards + 1 // Relative to list
  };
  toDos.push(todo);
  ++list.cards;
  return card;
}

function findListObject(array, ID, isDashed) {
  function getArrayObjectById(object) {
    if (isDashed) {
      return object._id === ID;
    } else return object.id === ID;
  }
  let arrayOfObjects = Array.from(array);
  arrayOfObjects.forEach(element => {});
  return arrayOfObjects.find(getArrayObjectById);
}

function addTodo(cardDOM, listID, index) {
  listID = listID || 1;
  if (!cardDOM) return false;
  let listNodes = UI.elBoard.childNodes;
  let list = findListObject(listNodes, `list_${listID}`, false);
  let card = createCard(cardDOM, listID, index);
  if (index) {
    list.insertBefore(card, list.children[index]);
  } else {
    list.appendChild(card);
  }
  updateCardCounts();
  // TODO save these into database with ajax
}

function addList(name) {
  name = name.trim();
  if (!name || name === '') return false;
  let newListID = ++_listCounter;
  let list = document.createElement('div');
  let heading = document.createElement('h3');
  let listCounter = document.createElement('span');

  list.dataset.id = newListID;
  list.id = 'list_' + newListID;
  list.className = 'list';
  list.appendChild(heading);

  heading.className = 'listname';
  heading.innerHTML = name;
  heading.appendChild(listCounter);

  listCounter.innerHTML = 0;

  lists.push({
    _id: newListID,
    name: name,
    cards: 0,
    elCounter: listCounter
  });

  UI.elBoard.append(list);
}

// Update Card Counts on drop
function updateCardCounts() {
  lists.map(list => {
    list.elCounter.innerHTML = list.cards;
  });
}

function moveCard(cardId, newListId, index) {
  if (!cardId) return false;
  try {
    let card = findListObject(toDos, cardId, true);
    if (card.listID !== newListId) {
      // If dropped on different list
      --findListObject(lists, card.listID, true).cards;
      card.listID = newListId;
      ++findListObject(lists, card.listID, true).cards;
      updateCardCounts();
    }

    if (index) {
      card.index = index;
    }
  } catch (e) {
    console.log(e.message);
    alert(`Error ${e} caught!`);
  }
}

function getCardPlaceholder() {
  if (!UI.elCardPlaceholder) {
    // Create on inexistence
    UI.elCardPlaceholder = document.createElement('div');
    UI.elCardPlaceholder.className = 'card-placeholder';
  }
  return UI.elCardPlaceholder;
}

//Called on domcontentload event
function loadListsAndCards(listArray, cardsArray) {
  appendElBoardOnElementById();
  createBoard(listArray, cardsArray);

  //below is for development only
  //writeOutListsAndTodos()
}

function createBoard(listArray, cardsArray) {
  listArray.forEach(listName => {
    addList(listName);
  });
  cardsArray.forEach(card => {
    addTodo(createCardDOMFromText(card), card.listId, card.index);
  });
}

//Load lists and content of lists
document.addEventListener('DOMContentLoaded', () => {
  loadListsAndCards(sampleListArray, sampleCardArray); //these are samples from data.js
});

function createKanbanDiv() {
  let kanbanDiv = document.createElement('div');
  kanbanDiv.id = 'kanban_board';
  kanbanDiv.className = 'kanban-board';
  return kanbanDiv;
}

//Generating card DOM from cardObject
function createCardDOMFromText(cardObject) {
  let cardDOM = `Name: ${cardObject.name}`;
  return cardDOM;
}

// Append kanban DOM to a specific ID
function appendElBoardOnElementById(id = 'kanban_id') {
  let parentDomElement = document.getElementById(id);
  parentDomElement.appendChild(UI.elBoard);
}

//below is for development only
function writeOutListsAndTodos() {
  lists.forEach(function(listMember) {
    console.log(listMember);
  });
  toDos.forEach(function(toDosMember) {
    console.log(toDosMember);
  });
}
