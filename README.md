#Hi!

This is a kanban function that generates a kanban board based on given array of lists and given array of cards in it.

##After cloning:##

###Make sure You have node.js installed on Your computer or laptop. If You haven't, install it.###

Next:

1. run `cd kanban_function/`

2. run `npm i`

3. edit variables inside **data.js**, **sampleListArray** variable holds kanban lists, **sampleCardArray** variable holds cards

4. each must have an id referencing the list it pertains, named **listId**, and an index referencing it's place inside the list, named **index** leaving **data.js** file however default causes no problem

5. run `gulp build`

6. run `node index.js`

7. go to **localhost:3000** inside preferred browser

8. You should see the kanban board
