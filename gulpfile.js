const gulp = require('gulp');
const terser = require('gulp-terser');
const cleanCSS = require('gulp-clean-css');
const htmlmin = require('gulp-htmlmin');

gulp.task('default', () => {
  console.log('Gulp is running correctly!');
});

gulp.task('uglify', () => {
  gulp
    .src('public/*.js')
    .pipe(terser())
    .pipe(gulp.dest('dist'));
});

gulp.task('minify-css', () => {
  return gulp
    .src('public/*.css')
    .pipe(cleanCSS({ compatibility: 'ie8' }))
    .pipe(gulp.dest('dist'));
});

gulp.task('minify', () => {
  return gulp
    .src('public/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist'));
});

gulp.task('build', ['uglify', 'minify-css', 'minify']);
